# SOAL NO 1

Mampu mendemonstrasikan perancangan basis data berdasarkan permasalahan dunia nyata melalui reverse engineering produk digital global. Lampirkan bukti tabel hasil desain.

# JAWABAN NO 1

Pada desain ERD dari produk digital Gofood ini memiliki 11 entitas, diantaranya :
user, menu, toko , kategori (untuk kategori produk), pesanan, detail_pesanan, metode_pembayaran, jenis_pengiriman, pengiriman, pengemud dan kendaraan (untuk pengemudi).

![desain_database_gofood](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/ERD_GOFOOD.png)

# SOAL NO 2

Mampu mendemonstrasikan DATA DEFINITION LANGUAGE (DDL) secara tepat berdasarkan minimal 10 entitas dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record.

# JAWABAN NO 2

### (0) create database
Screenshot :

![membuat_database_gofood](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/(0)%20Create%20DB.png)

### (1) tabel user
Screenshot :

![membuat_tabel_user](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20User.png)

Unique user :

![unique_user](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/Unique%20User.png)

Screen Record :

![screen_record_user](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20user.gif)

### (2) tabel toko
Screenshot :

![membuat_tabel_toko](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Toko.png)

Screen Record :

![screen_record_toko](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20toko.gif)

### (3) tabel menu
Screenshot :

![membuat_tabel_menu](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Menu.png)

Foreign Key :

![fk_menu](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/Foreign%20Key%20Menu.png)

Screen Record :

![screen_record_menu](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20menu.gif)

### (4) tabel kategori
Screenshot :

![membuat_tabel_kategori](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Kategori.png)

Screen Record :

![screen_record_kategori](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20kategori.gif)

### (5) tabel pesanan
Screenshot :

![membuat_tabel_pesanan](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Pesanan.png)

Foreign Key :

![fk_pesanan](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/Foreign%20Key%20Pesanan.png)

Screen Record :

![screen_record_pesanan](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20pesanan.gif)

### (6) tabel detail_pesanan
Screenshot :

![membuat_tabel_detail_pesanan](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Detail%20Pesanan.png)

Foreign Key :

![fk_detail_pesanan](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/Foreign%20Key%20Detail%20Pesanan.png)

Screen Record :

![screen_record_detail_pesanan](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20detail_pesanan.gif)

### (7) tabel jenis_pengiriman
Screenshot :

![membuat_tabel_jenis_pengiriman](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Jenis%20Pengiriman.png)

Screen Record :

![screen_record_jenis_pengiriman](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20jenis_pengiriman.gif)

### (8) tabel metode_pembayaran
Screenshot :

![membuat_tabel_metode_pembayaran](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Metode%20Pembayaran.png)

Screen Record :

![screen_record_metode_pembayaran](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20metode_pembayaran.gif)

### (9) tabel pengiriman
Screenshot :

![membuat_tabel_pengiriman](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Pengiriman.png)

Foreign Key :

![fk_pengiriman](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/Foreign%20Key%20Pengiriman.png)

Screen Record :

![screen_record_pengiriman](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20pengiriman.gif)

### (10) tabel pengemudi
Screenshot :

![membuat_tabel_pengemudi](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Pengemudi.png)

Foreign Key :

![fk_pengemudi](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/Foreign%20Key%20Pengemudi.png)

Screen Record :

![screen_record_pengemudi](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20pengemudi.gif)

### (11) tabel kendaraan
Screenshot :

![membuat_tabel_kendaraan](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/DDL/TB%20Kendaraan.png)

Screen Record :

![screen_record_kendaraan](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/tb%20kendaraan.gif)

# SOAL 3
Mampu mendemonstrasikan DATA MANIPULATION LANGUAGE (DML) berdaraskan minimal 5 use case operasional (CRUD) dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record.

# JAWABAN NO 3
Screen Record :

![jawaban_no_3](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/dml%20crud.gif)

# SOAL 4
Mampu mendemonstrasikan DATA QUERY LANGUAGE (DQL) berdaraskan minimal 5 pertanyaan analisis dari produk digital yang dipilih, menggunakan setidaknya keyword GROUP BY, INNER JOIN, LEFT / RIGHT JOIN, AVERAGE / MAX / MIN. Lampirkan bukti berupa source code dan screen record.

# JAWABAN NO 4
Screen Record :

![screen_record_jawaban_4](https://gitlab.com/arhamsyuhada1/uts-praktikum-basis-data/-/raw/main/GIF/dql.gif)

# JAWABAN NO 5
